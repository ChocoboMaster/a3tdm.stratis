class A3TDM_Core
{
	tag = "A3TDM";
	
	class Master_Directory
	{
		file = "core";
		class setupActions {};
		class initPlayer {};
		class statusBar {};
		class welcomeNotification {};
	};

	class Functions
	{
		file = "core\functions";
		class setupHud {};
		class setupArsenal {};
		class setupSafeZone {};
		class jumpScript {};
	};

	class GroupSystem
	{
		file = "core\group";
		class inviteToGroup {};
		class kickFromGroup {};
		class leaveGroup {};
		class acceptGroupInvite {};
		class declineGroupInvite {};
		class disbandGroup {};
	};

	class EventHandler
	{
		file = "core\events";
		class setupEventHandlers {};
		class onPlayerKilled {};
		class handleDamage {};
		class onPlayerRespawn {};
		class onFired {};
	};
};