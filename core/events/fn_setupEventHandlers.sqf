/*
Master eventhandler file
*/
player addEventHandler["Killed", {_this call A3TDM_fnc_onPlayerKilled}];
player addEventHandler["handleDamage",{_this call A3TDM_fnc_handleDamage;}];
player addEventHandler["Respawn", {_this call A3TDM_fnc_onPlayerRespawn}];
player addEventHandler["Fired",{_this call A3TDM_fnc_onFired}];