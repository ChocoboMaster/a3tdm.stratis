/*
	File: fn_updateHud.sqf
	Author: Charles "MaitreChocobo" B.
	
	Description:
	Initializes the Player.
*/
RemoveAllWeapons player;
{player removeMagazine _x;} foreach (magazines player);
removeUniform player;
removeVest player;
removeBackpack player;
removeGoggles player;
removeHeadGear player;
{
	player unassignItem _x;
	player removeItem _x;
} foreach (assignedItems player);

//Load player with default cop gear.
player addWeapon "arifle_MXM_F";
player addUniform "U_BG_Guerilla1_1";
player addVest "V_TacVest_blk";
player addMagazine "30Rnd_65x39_caseless_mag";
player addMagazine "30Rnd_65x39_caseless_mag";
player addMagazine "30Rnd_65x39_caseless_mag";
player addMagazine "30Rnd_65x39_caseless_mag";
player addWeapon "hgun_Pistol_heavy_02_Yorris_F";
//TODO Add Magazines to Pistol
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "Laserdesignator";
player assignItem "Laserdesignator";