/*
	File: fn_statusBar.sqf
	Author: Charles "MaitreChocobo" B.
	
	Description:
	Initializes and shows the status bar.
*/

waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "statusBar" call BIS_fnc_rscLayer;
_rscLayer cutRsc["statusBar","PLAIN"];
systemChat format["A3TDM Stratis v0.1", _rscLayer];

[] spawn {
	sleep 5;
	_statusText = "A3TDM Stratis";
	_counter = 180;
	_timeSinceLastUpdate = 0;
	while {true} do
	{
		sleep 1;
		_counter = _counter - 1;
		_statusText = "A3TDM v0.1";
		((uiNamespace getVariable "statusBar")displayCtrl 1000)ctrlSetText format["%3 | FPS: %1 | Joueur connecté : %2   ", round diag_fps, count playableUnits, _statusText, _counter];
	};
};