/*
	File: fn_setupArsenal.sqf
	Author: Charles "MaitreChocobo" B.
	
	Description:
	Initializes the arsenal.
*/
if (isDedicated) exitWith {};
if !(hasinterface) exitwith {};

#define SAFETY_ZONES    [["spawn_1", 15], ["spawn_2", 15]]

["Preload"] call BIS_fnc_arsenal;

/*if ({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then {
	Player addAction ["<t color='#FF0000'>Arsenal</t>", "['Open', true] call BIS_fnc_arsenal"];
};*/
Player addAction ["<t color='#FF0000'>Arsenal</t>", "['Open', true] call BIS_fnc_arsenal"];

[] spawn {
	waitUntil {!isNull player && player == player};
	waitUntil{!isNil "BIS_fnc_init"};
	waituntil {!(IsNull (findDisplay 46))};
	
	private["_i", "_keyDown"];
   	_keyDown = (findDisplay 46) displayAddEventHandler ["KeyDown", "if (_this select 1 == 23 && {(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then {['Open', true] call BIS_fnc_arsenal;}"];
};