/*
	File: fn_setupHud.sqf
	Author: Charles "MaitreChocobo" B.
	
	Description:
	Initializes the HUD.
*/
private["_display","_alpha","_version","_p","_pg"];
disableSerialization;
_display = findDisplay 46;

2 cutRsc ["A3TDM_hud","PLAIN"];
/*[] call life_fnc_hudUpdate;

[] spawn
{
	private["_dam","_cash"];
	while {true} do
	{
		_dam = damage player;
		_cash  = life_cash+life_atmcash;
		waitUntil {((damage player) != _dam) || _cash != life_cash+life_atmcash};
		[] call life_fnc_hudUpdate;
	};
};*/