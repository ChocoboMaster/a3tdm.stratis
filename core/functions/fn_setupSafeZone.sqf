/*
	File: fn_setupSafeZone.sqf
	Author: Charles "MaitreChocobo" B.
	
	Description:
	Initializes the safezones (spawn).
*/

#define SAFETY_ZONES    [["spawn_1", 15], ["spawn_2", 15]] //Safe zone configuration
#define MESSAGE "Il est interdit de tirer dans les Spawn!"

if (isDedicated) exitWith {};
waitUntil {!isNull player};

player addEventHandler ["Fired", {
    if ({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then {
        deleteVehicle (_this select 6);
        titleText [MESSAGE, "PLAIN", 1];
    };
}];

player addEventHandler ["HandleDamage", {
    player setDamage 1;
}];
