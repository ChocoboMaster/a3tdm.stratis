class RscTitles
{
	class A3TDM_hud
	{
		idd=-1;
		fadein=0;
		duration=99999999999999999999999999999999999999999999;
		movingEnable=0;
		enableSimulation=1;
		enableDisplay = 1;
		fadeout=0;
		name="A3TDM_hud";
		onLoad="uiNamespace setVariable ['A3TDM_hud',_this select 0]";
		objects[]={};

		class controls
		{
			class RscText1
			{
				idc = 1100;
				text = "25";
				x = safezoneX + safezoneW/2 - 0.04;
				y = safezoneY + 0.03;
				w = 0.07;
				h = 0.055;
				colorBackground[] = {191,-0,0,0.5};
				font = "PuristaSemibold";
				size = 0.05;
				type = 13;
				style = 1;
				class Attributes {
					align="center";
					color = "#FFFFFF";
				};
			};
			class RscText2
			{
				idc = 1101;
				text = "25";
				x = safezoneX + safezoneW/2 + 0.04;
				y = safezoneY + 0.03;
				w = 0.07;
				h = 0.055;
				colorBackground[] = {0,0,191,0.5};
				font = "PuristaSemibold";
				size = 0.05;
				type = 13;
				style = 1;
				class Attributes {
					align="center";
					color = "#FFFFFF";
				};
			};
		};   
	};	
};